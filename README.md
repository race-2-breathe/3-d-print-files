# 3-D print files - Race 2 breathe

Repository for all Race 2 Breathe 3-d print files available on [race-2-breathe.com](https://race-2-breathe.com) and the
[race-2-breathe app](https://app.race-2-breathe.com).

Licencing is per-directory as this is a summary of multiple collections. please
respect the license contained in each directory.

If you are a medic and want to order 3-d printed parts. Please register on [https://race-2-breathe.com/join](https://race-2-breathe.com/join)
If you are a maker and you want to contribute in our initiative please register on [https://race-2-breathe.com/join](https://race-2-breathe.com/join)

## Table of Contents

- [Questions? Feedback?](#questions-feedback)
- [Folder Structure](#folder-structure)
- [Available designs](#available-designs)
  - [4 hole Faceshield](#4-hole-faceshield)
  - [8 hole Faceshield](#8-hole-faceshield)
- [Legal Disclaimer](#legal-disclaimer)

## Questions Feedback

We are always looking for new proven designs. If you have a new design that is
better and validated, don't hesitate to reach out to us. We will add your design
to this repo and the Race-2-breathe application and website. Contact us via our
[website](https://race-2-breathe.com/about-us/contact).

All designs in this repo are printable and actively used by the Race-2-breathe
application.

Fair warning, not every design is validated for all countries, Please verify if
a design is legally allowed to avoid legal issues.

## Folder Structure

```
.
├── [Design Subfolders]
│    ├── [....]
│    ├── README.md
│    └── LICENCE.md
├── README.md
└── LICENCE.md
```

## Available Designs

### 4 hole Faceshield

The four-hole face-shield is printable with any PLA 3d-printer. The transparent
parts are randomly available. The mask has a short printing time and is good for
mass production.

### 8 hole Faceshield

The eight-hole face-shield is printable with any PLA 3d-printer. The transparent
parts are randomly available. The mask has a short printing time and is good for
mass production.

## Legal Disclaimer

Race-2-breathe is not responsible for any loss of life, damages or other
liabilities resulting from the use of these products. We recommend using
professional approved equipment where possible.

None of these products has been formally medically tested. Use it at your own
risk.
